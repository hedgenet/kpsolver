from setuptools import setup

required_modules = []

with open('REQUIREMENTS.txt','r') as f:
    required_modules = list([l.strip() for l in f])

setup(
    name='kpsolver',
    version='0.0.2',
    author='Jonathan Pelletier',
    author_email='jonathan.pelletier1@gmail.com',
    packages=['kpsolver', 'kpsolver.tests'],
    scripts=['knapsolve.py'],
    url='https://gitlab.com/hedgenet/kpsolver',
    license='LICENSE.txt',
    description='A knapsack problem solver with a simple interface that uses a memoized, top-down algorithm',
    long_description=open('doc/USAGE.rst').read(),
    install_requires=required_modules,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering :: Mathematics'
        ]
)

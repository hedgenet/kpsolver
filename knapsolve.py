#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv
import numpy as np

from kpsolver import dynamicp as solve

def main():
    """ computes the optimal solution to the knapsack problem """

    # command line argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '-i',
            '--items',
            help='path to the csv file containing the items of the knapsack'\
                    ' problem instance',
            required=True,
            type=argparse.FileType('r')
            )

    parser.add_argument(
            '-c',
            '--capacity',
            help='Capacity of the knapsack problem instance',
            required=True,
            type=int
            )

    delimiter = ','
    parser.add_argument(
            '-d',
            '--delimiter',
            help='delimiter used in the items file',
            default=delimiter
            )

    print_val = False
    parser.add_argument(
            '-p',
            '--print-val',
            help='print the optimal value of the knapsack or not',
            action='store_true',
            default=print_val
            )

    quote = '"'
    parser.add_argument(
            '-q',
            '--quote',
            help='quote char used in the items file',
            default=quote
            )

    args = parser.parse_args()

    # parse the items into a list
    reader = csv.reader(args.items,
            delimiter=args.delimiter,
            quotechar=args.quote)

    # read the items from the csv
    file_items = np.array([ i for i in reader ])

    # parse file_items into something the solver understands
    items = np.array(file_items[:,1:], dtype=float)


    V, sol = solve(items, args.capacity)

    sol_slicer = np.array(sol) == 1
    selected_items = file_items[sol_slicer]

    if args.print_val:
        print(V)

    for s in selected_items:
        print(args.delimiter.join(s))

    return

if __name__ == '__main__':
    main()

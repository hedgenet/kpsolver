# kpsolver
kpsolver is a knapsack problem solver with a simple interface that uses a 
memoized, top-down programming algorithm.

## Installation

```bash
pip install kpsolver
```

## Usage

### CLI
The solver comes with a utility that can be used from the command line to solve
knapsack problems that have the right format (see the Problem file format for
details). Here is an example invocation:

```bash
knapsolve --items /path/to/problem/file/items --capacity 8
```

### Package
The solver can also be used as a python module. The dynamicp function uses a 
dynamic programming algorithm to compute the solution to the problem and takes
as input the variables *K* and *items* where:

K = the capacity of the knapsack problem instance

items = a list of tuples representing the items of the knapsack problem 
instance. Each tuple is of the form (value, weight) where value and weight 
represent the value and the weight of the items respectively.

Here is an example use of the module:
```python
>>> from kpsolver import dynamicp as solver
>>> items = [ (15,1), (10,5), (9,3), (5,4) ]
>>> K = 8
>>> solver(items, K)
(29, [1, 0, 1, 1])
```

## Problem file format
The utility bundled with the package assumes the following format for 
knapsack problem items file:

> description1,value1,weight1 
>
> ...
>
> descriptionN,valueN,weightN

Here is a possible example file:

> SHORTGOOGL18Aug17P955, 69.15, 28149.75                                          
> SHORTTSLA18Aug17C350, 110.27, 10876.20                                          
> SHORTBABA18Aug17C152, 221.91, 4731.75                                           
> SHORTBABA18Aug17C160, 251.21, 4731.75                                           
> SHORTFEYE18Aug17P14, 0.37, 418.95                                               
> SHORTFB18Aug17C165, 136.70, 5126.25

In case you are wondering, the items in this particular example file
reprensent options contract on the following securities: Google, Tesla Motors, 
Alibaba Group Holding ,FireEye Inc and Facebook. The value of an item 
reprensents the expected value, computed from a proprietary model, of short 
selling the option contract. The weight represents the brokerage margin 
requirement for opening the position.

for more information on the application of the knapsack problem to investment,
visit [thearchidux.com](www.thearchidux.com/2017/08/16/knapsack-investment/)

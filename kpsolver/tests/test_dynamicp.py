from kpsolver import dynamicp

class TestDynamicp1:

    def setUp(self):

        self.items = [
                (15, 1),
                (10, 5),
                (9, 3),
                (5, 4)
                ]

        self.K = 8
        return

    def tearDown(self):
        pass

    def test_dynamicp1(self):
        """ function should return the optimal knapsack """
        expected = (29, [1, 0, 1, 1])
        result = dynamicp(self.items, self.K)

        assert expected == result
        return

class TestDynamicp2:

    def setUp(self):

        self.items = [
                (4, 12),
                (2, 1),
                (6, 4),
                (1, 1),
                (2, 2)
                ]

        self.K = 15
        return

    def test_dynamicp2(self):
        """ function should return the optimal knapsack """
        expected = (11, [0, 1, 1, 1, 1])
        result = dynamicp(self.items, self.K)

        assert expected == result
        return

